var data = require("sdk/self").data;
var pageMod = require("sdk/page-mod");

pageMod.PageMod (
{
	include: ["https://www.youtube.com*", "http://www.youtube.com*"],
	contentScriptWhen: 'ready',
	contentScriptFile: [data.url("jquery-1.9.0.min.js"), data.url("addTitles.js")]
});

